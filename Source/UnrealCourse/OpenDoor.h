// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

class ATriggerVolume;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UNREALCOURSE_API UOpenDoor : public UActorComponent
{
   GENERATED_BODY()

public:
   // Sets default values for this component's properties
   UOpenDoor();

protected:
   // Called when the game starts
   virtual void BeginPlay() override;

   void OpenDoor(); 
   void CloseDoor();

public:
   // Called every frame
   virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
   UPROPERTY(EditAnywhere)
   float OpenAngle = 90.f;

   UPROPERTY(EditAnywhere)
   ATriggerVolume* PressurePlate;

   UPROPERTY(EditAnywhere)
   float DoorCloseDelay = 0.1f;
   float mLastDoorOpenTime;

   //UPROPERTY(EditAnywhere)
   AActor* ActorThatOpens;

   AActor* mOwner;
};
