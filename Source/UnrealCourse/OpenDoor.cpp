// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "Engine/TriggerVolume.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
   // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
   // off to improve performance if you don't need them.
   PrimaryComponentTick.bCanEverTick = true;
   mOwner = GetOwner();
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
   Super::BeginPlay();

   ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
}

void UOpenDoor::OpenDoor()
{
   mOwner->SetActorRotation(FRotator(0.f, OpenAngle, 0.f));
}

void UOpenDoor::CloseDoor()
{
   mOwner->SetActorRotation(FRotator(0.f, 0.f, 0.f));
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
   Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

   if (PressurePlate && PressurePlate->IsOverlappingActor(ActorThatOpens))
   {
      OpenDoor();
      mLastDoorOpenTime = GetWorld()->GetTimeSeconds();
   }
   
   if (mLastDoorOpenTime + DoorCloseDelay < GetWorld()->GetTimeSeconds())
      CloseDoor();
}

